import pandas
import numpy as np
from sklearn.linear_model import LinearRegression

data_df = pandas.read_csv("depenses_anonymes.csv", sep=',')

x = np.array(data_df["salaire"].to_list())
y = np.array(data_df["depenses"].to_list())

lr = LinearRegression()

lr.fit(x.reshape(-1, 1), y)

result_data = {
    "salaire": x,
    "depenses": y,
    "prediction_depense": ["".join(str(a) for a in lr.predict([[i]])) for i in x]
}

result_df = pandas.DataFrame(data=result_data)
result_df.to_csv("predictions_1.csv", index=False)

###################

data2_df = pandas.read_csv("depenses_anonymes.csv", sep=',')

x2 = [data2_df["salaire"].to_list(), data2_df["age"].to_list()]
x2 = np.array(x2)

y2 = np.array(data2_df["depenses"].to_list())

lr2 = LinearRegression()

lr2.fit(x2.T, y2)

result_df["prediction2_depense"] = [
    "".join(str(a) for a in lr2.predict([[i, j]])) for i, j in zip(x2[0], x2[1])]

result_df.to_csv("predictions_2.csv", index=False)
