import pandas

class Standardizer:
    name_index = 0
    city_index = 0

    def __init__(self, csv_file):
        self.data_df = pandas.read_csv(csv_file, sep=',')
        self.__anonymize_names()
        self.__anonymize_cities()
        self.__standardize()

    def __anonymize_names(self, column_name="nom"):
        self.data_df[column_name] = self.data_df[column_name].apply(
            lambda value:
                self.__create_anonymous_name()
        )

    def __anonymize_cities(self, column_name="ville"):
        city_set = set(self.data_df[column_name].to_list())
        self.data_df[column_name] = self.data_df[column_name].replace(
            city_set, ["ville" + str(i) for i in range(1, len(city_set) + 1)]
        )

    def __create_anonymous_name(self):
        self.name_index = self.name_index + 1
        return "id" + str(self.name_index)

    def __standardize(self):
        for i in ["age", "salaire", "depenses"]:
            self.data_df[i] = self.data_df[i].sub(self.data_df[i].mean())
            self.data_df[i] = self.data_df[i].div(self.data_df[i].std())

    def data(self):
        return self.data_df


if __name__ == "__main__":
    Standardizer("depenses.csv").data().to_csv("depenses_anonymes.csv", index=False)